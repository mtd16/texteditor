from tkinter import Tk, scrolledtext, Menu, filedialog, END, messagebox, simpledialog
import os

# Root for main window
root = Tk(className = " Text Editor")
textArea = scrolledtext.ScrolledText(root, width=100, height=80)


### FUNCTIONS ###

def newFile():
    # Check if file contains data
    if len(textArea.get('1.0', END+'-1c')) > 0:
        if messagebox.askyesno("Save?", "Do you want to save file?"):
            saveFile()
       
        else:
            textArea.delete('1.0', END)
    
    root.title("Text Editor")  

def openFile():
    file = filedialog.askopenfile(parent=root, title='Select a text file', filetypes=(("Text File", "*.txt"), ("All files", "*.*")))

    root.title(os.path.basename(file.name) + " - Text Editor")

    if file != None:
        contents = file.read()
        textArea.insert('1.0', contents)
        file.close()

def saveFile():
    file = filedialog.asksaveasfile(mode='w', defaultextension=".txt", filetypes=(("HTML file", "*.html"), ("Text file", "*.txt"), ("All files", "*.*")))

    if file != None:
        # slice off the last character from get, as an extra return (enter) is added
        data = textArea.get('1.0', END+'-1c')
        file.write(data)
        file.close()

def findInFile():
    findString = simpledialog.askstring("Find...", "Enter Text")
    textData = textArea.get('1.0', END)

    found = textData.upper().count(findString.upper())

    if textData.upper().count(findString.upper()) > 0:
        label = messagebox.showinfo("Results", '"' + findString + '"' +  " was found " + str(found) + " times.")

    else:
        label = messagebox.showinfo("Results", "Text not found.")

    print(textData.upper().count(findString.upper()))
def about():
    label = messagebox.showinfo("About", "A simple homemade Python text editor. Enjoy!")

def exitRoot():
    if messagebox.askyesno("Quit", "Are you sure?"):
        root.destroy()

def FontHelvetica()
    global text
    text.config(font="Helvetica")

def FontCourier():
    global text
    text.config(font="Courier")


# Menu options
menu = Menu(root)
root.config(menu=menu)

fileMenu = Menu(menu)

menu.add_cascade(label="File", menu=fileMenu)
fileMenu.add_command(label="New", command=newFile)
fileMenu.add_command(label="Open", command=openFile)
fileMenu.add_command(label="Save", command=saveFile)
fileMenu.add_command(label="Find", command=findInFile)
fileMenu.add_command(label="Print")
fileMenu.add_separator()
fileMenu.add_command(label="Exit", command=exitRoot)

helpMenu = Menu(menu)
aboutMenu = Menu(menu)
menu.add_cascade(label="Help", menu=helpMenu)
menu.add_cascade(label="About", menu=aboutMenu)
aboutMenu.add_command(label="Info", command=about)



textArea.pack()

# Keep window open
root.mainloop()